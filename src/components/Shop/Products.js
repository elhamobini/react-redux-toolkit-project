import ProductItem from "./ProductItem";
import classes from "./Products.module.css";

const DUMMY_ITEMS = [
  {
    id: "p1",
    title: "first book",
    price: 10,
    decription: "this is a first book!",
  },
  {
    id: "p2",
    title: "second book",
    price: 20,
    decription: "this is a second book!",
  },
];

const Products = (props) => {
  return (
    <section className={classes.products}>
      <h2>Buy your favorite products</h2>
      <ul>
        {DUMMY_ITEMS.map((item) => (
          <ProductItem
            key={item.id}
            id={item.id}
            title={item.title}
            price={item.price}
            description={item.decription}
          />
        ))}
      </ul>
    </section>
  );
};

export default Products;
