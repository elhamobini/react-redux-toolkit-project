const { createSlice } = require("@reduxjs/toolkit");

const uiSlice = createSlice({
  name: "ui",
  initialState: { isCartVisited: false, notification: null },
  reducers: {
    toggle(state) {
      state.isCartVisited = !state.isCartVisited;
    },
    showNotification(state, action) {
      state.notification = {
        status: action.payload.status,
        title: action.payload.title,
        message: action.payload.message,
      };
    },
  },
});

export const { toggle, showNotification } = uiSlice.actions;

export default uiSlice.reducer;
