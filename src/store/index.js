import { configureStore } from "@reduxjs/toolkit";
import uiSlice from "./slices/ui";
import cartSlice from "./slices/cart";

const store = configureStore({
  reducer: {
    ui: uiSlice,
    cart: cartSlice,
  },
});

export default store;
